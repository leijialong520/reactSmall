/**
 * 首页顶部导航栏
 */


let homeTopNavs,isLogin="inline-block",getTypeData=[],getCommodityData;
function HomeTopNavs(){
	homeTopNavs = ReactDOM.render(
			<HomeTopNav></HomeTopNav>,
			document.getElementById('head')
	);
	$.ajax({
		url:CTX_PATH+'/isLogin',
		type:'post',
		success:function(rtn){
			console.log('is login data:%o',rtn);
			if(!rtn.error){
				homeTopNavs.setState({login:rtn.data});
				isLogin="none";
			}else{
				layer.msg(rtn.msg);
			}
		}
	});
}

function RendRs(){
	$.ajax({
		url:CTX_PATH+"/shop/api/banners",
		type:'post',
		success:function(rtn){
			if(!rtn.error){
				ReactDOM.render(
					<RoundSowing datas={rtn.data}></RoundSowing>,
					document.getElementById("round"),
				)
			}
			
		}
	});
	
	
}

let AA,pageNum=1,pageSize=8,BB,data1=[{mainUrl:"../home",name:"平台首页",data:[{url:"https://www.teraee.com/?page_id=36090",name:"最新资讯"},
                                                          {url:CTX_PATH+'/solution',name:"申请产品试用"},
                                                          {url:"https://www.teraee.com/?page_id=37047",name:"在线演示"}]},
                  {mainUrl:"./order/detail",name:"我的订单"},{mainUrl:"./address",name:"地址管理"}
                  ];
const getType=new Promise((resolve, reject) =>{
	$.ajax({
		url:CTX_PATH+"/shop/api/category/all",
		type:'post',
		success:function(rel){
			if(!rel.error){
				resolve(rel.data);
			}
		}
	});
})
const getCommodity=new Promise((resolve, reject) =>{
	$.ajax({
		url:CTX_PATH+"/shop/api/goods/page",
		type:'post',
		data:{
			pageNum:pageNum,
			pageSize:pageSize
				},
		success:function(rel){
			if(!rel.error){
				++pageNum;
				resolve(rel.data.list);
			}
		}
	});
})
function ADDcommodity(){
	Promise.all([getType, getCommodity]).then(function (rel) {
		for(let i=0;i<rel[0].length;i++){
			getTypeData=getTypeData.concat(rel[0][i].childs);
		}
		getCommodityData=rel[1];	
		AA=ReactDOM.render(
				<CommodityBody datas={getCommodityData} lables={getTypeData}></CommodityBody>,
				document.getElementById("commodity"),
		)	});
	
}

function AddBuyCar(){
	$.ajax({
		url:CTX_PATH+"/shop/api/cart/goods/page",
		type:'post',
		data:{
			pageNum:"1",
			pageSize:"100"
				},
		success:function(rel){
			if(!rel.error){
				BB=ReactDOM.render(		
						<BuyCar datas={rel.data.list}></BuyCar>,
						document.getElementById("buyCarInRight")
				)
			}else{
				layer.msg(rel.msg);
			}
		}
	});
}
let finished = true,WindowUuid,priceMin,priceMax;
$(window).scroll(function() {
	if($("body").get(0).scrollHeight-$(window).height()-$(this).scrollTop()<30&&finished){
		finished=false
		$.ajax({
			url:CTX_PATH+"/shop/api/goods/page",
			type:'post',
			data:{
				pageNum:pageNum,
				pageSize:pageSize,
				categoryUuid:WindowUuid,
				priceMin:priceMin,
				priceMax:priceMax
					},
			success:function(rel){
				if(!rel.error){
					if(rel.data.list.length>0){
						++pageNum;
					getCommodityData=getCommodityData.concat(rel.data.list);
					AA.aloadingMore(getCommodityData);
					}
				}
			}
		});
	}
	});
function addNewBuy(data){
	
		$.ajax({
			url:CTX_PATH+"/shop/api/cart/add",
			type:'post',
			data:{
				goodsUuid:data.id,
				num:1
					},
			success:function(rel){
				if(!rel.error){
					layer.msg("成功加入购物车！");
					BB.count(1,data.price)
					let result = BB.updata([]);
					havClass=[];
					result.done(()=>{
						getNewBuyData()
					})		
					}else{
					layer.msg(rel.msg);
				}
			}
		});
		/*//下架购物车都方法
		let s=havClass;
		havClass=[];
		BB.count(-s[index].Nnum,data[0].price)
		s.splice(index,1);
		let result = BB.updata([]);
		result.done(()=>{
			BB.updata(s)
		})*/
		
		/*let s=havClass;
		havClass=[];
		++s[0].num;
		BB.count(1,data.price)
		let result = BB.updata([]);
		result.done(()=>{
			BB.updata(s)
		})
	*/
}
function getNewBuyData(){

	$.ajax({
		url:CTX_PATH+"/shop/api/cart/goods/page",
		type:'post',
		data:{
			pageNum:"1",
			pageSize:"100"
				},
		success:function(rel){
			if(!rel.error){
				BB.updata(rel.data.list)
			}else{
				layer.msg(rtn.msg);
			}
		}
	});	
}
(function(){
 	ReactDOM.render(			
		<SidebarNav data={data1}></SidebarNav>,
		document.getElementById("SGDsidebarNav")
		)
})();
HomeTopNavs();
RendRs();
ADDcommodity();
AddBuyCar();