
/**
 * 场景选择表单
 */
var SceneForm = React.createClass({
	render:function(){
		return <form className='solution-bot-form'>
					<div className='field-box'>
				        <label className='form-name'>单 位：</label>
				        <label >
					        <input type='radio' value='1' name='sceneUnit' checked='checked' /> <span >中小学</span>
				        </label>
					    <label >
					        <input type='radio' value='2' name='sceneUnit' /> <span >高校</span>
				        </label>
					    <label >
					        <input type='radio' value='3' name='sceneUnit' /> <span >企业</span>
				        </label>
						<label >
					        <input type='radio' value='4' name='sceneUnit' /> <span >其他</span>
				        </label>
				    </div>
					<div className='field-box'>
				        <label className='form-name'>用 途：</label>
				        <label >
					        <input type='radio' value='1' name='sceneApplication' checked='checked' /> <span >多媒体教室</span>
				        </label>
					    <label >
					        <input type='radio' value='2' name='sceneApplication' /> <span >电脑教室</span>
				        </label>
					    <label >
					        <input type='radio' value='3' name='sceneApplication' /> <span >办公室</span>
				        </label>
						<label >
					        <input type='radio' value='4' name='sceneApplication' /> <span >电子阅览室</span>
				        </label>
						<label >
					        <input type='radio' value='5' name='sceneApplication' /> <span >云计算实训室</span>
				        </label>
				    </div>
				    <div className='field-box'>
				    	<label className='form-name'>单间点数：</label>
						<label >
					        <input type='radio' value='20-25' name='scenePoint' checked='checked' /> <span >20~25</span>
					    </label>
						<label >
					        <input type='radio' value='26-30' name='scenePoint' /> <span >26~30</span>
					    </label>
						<label >
					        <input type='radio' value='31-35' name='scenePoint' /> <span >31~35</span>
					    </label>
						<label >
					        <input type='radio' value='36-40' name='scenePoint' /> <span >36~40</span>
					    </label>
						<label >
					        <input type='radio' value='41-45' name='scenePoint' /> <span >41~45</span>
					    </label>
					    <label >
					        <input type='radio' value='46-50' name='scenePoint' /> <span >46~50</span>
					    </label>
						<label >
					        <input type='radio' value='51-55' name='scenePoint' /> <span >51~55</span>
					    </label>
						<label >
					        <input type='radio' value='56-60' name='scenePoint' /> <span >56~60</span>
					    </label>
				    </div>
			  </form>;
	}
});

var SoftHardWareForm = React.createClass({
	
	componentDidMount:function(){
		var $display = $(this.refs.display);
		$('input[name="terminal"]').change(function(){
			if($(this).val() == 'integratedMachine'){
				$display.attr('disabled','disabled');
				$display.prop('checked',false);;
			}else{
				$display.removeAttr('disabled');
			}
		});
	},
	render:function(){
		return <form className='solution-bot-form'>
					<div className='field-box'>
				    	<label className='form-name'>常用软件：</label>
				    	<label className='checkbox-inline'>
	                        <div className='checker' ><span><input name='softWare' type='checkbox' value='0' checked='checked' disabled='disabled'/><span>云管理软件</span></span></div> 
	                    </label>
				    	<label className='checkbox-inline'>
	                        <div className='checker' ><span><input name='softWare' type='checkbox' value='1' checked='checked' disabled='disabled'/><span> 操作系统</span></span></div>
	                    </label>
				    	<label className='checkbox-inline'>
	                        <div className='checker' ><span><input name='softWare' type='checkbox' value='2'/><span> AutoCAD</span></span></div> 
	                    </label>
					    <label className='checkbox-inline'>
	                        <div className='checker' ><span><input name='softWare' type='checkbox' value='3'/><span> Office</span></span></div> 
	                    </label>
						<label className='checkbox-inline'>
	                        <div className='checker' ><span><input name='softWare' type='checkbox' value='4'/><span> 3DMax</span></span></div> 
	                    </label>
						<label className='checkbox-inline'>
	                        <div className='checker' ><span><input name='softWare' type='checkbox' value='5'/><span> MatLAB</span></span></div> 
	                    </label>
						<label className='checkbox-inline'>
	                        <div className='checker' ><span><input name='softWare' type='checkbox' value='6'/><span> 编程软件</span></span></div> 
	                    </label>
						<label className='checkbox-inline'>
	                        <div className='checker' ><span><input name='softWare' type='checkbox' value='7'/><span> 语音教学软件</span></span></div> 
	                    </label>
					</div>
					<div className='field-box'>
						<label className='form-name'>终端：</label>
						<label >
					        <input type='radio' value='1' name='terminal' checked='checked' /> <span >x86终端</span>
					    </label>
						<label >
					        <input type='radio' value='2' name='terminal' /> <span >ARM终端</span>
					    </label>
						<label >
					        <input type='radio' value='3' name='terminal' ref='aio' /> <span >一体机</span>
					    </label>
					</div>
					<div className='field-box'>
						<label className='form-name'>配套硬件：</label>
				    	<label className='checkbox-inline'>
	                        <div className='checker' ><span><input name='hardWare' type='checkbox' value='1' checked='checked'/><span> 服务器</span></span></div> 
	                    </label>
					    <label className='checkbox-inline'>
	                        <div className='checker' ><span><input name='hardWare' type='checkbox' value='2' ref='display'  checked='checked'/><span> 显示器</span></span></div> 
	                    </label>
						<label className='checkbox-inline'>
	                        <div className='checker' ><span><input name='hardWare' type='checkbox' value='3'  checked='checked'/><span> 键鼠套件</span></span></div>
	                    </label>
						<label className='checkbox-inline'>
	                        <div className='checker' ><span><input name='hardWare' type='checkbox' value='4' checked='checked'/><span> 路由器</span></span></div> 
	                    </label>
						<label className='checkbox-inline'>
	                        <div className='checker' ><span><input name='hardWare' type='checkbox' value='5' checked='checked'/><span> 交换机</span></span></div> 
	                    </label>						
	                    <label className='checkbox-inline'>
	                        <div className='checker' ><span><input name='hardWare' type='checkbox' value='6' checked='checked'/><span> 机柜</span></span></div> 
	                    </label>
					    <label className='checkbox-inline'>
	                        <div className='checker' ><span><input name='hardWare' type='checkbox' value='7' checked='checked'/><span> UPS电源</span></span></div> 
	                    </label>
					</div>
					<div className='field-box'>
						<label className='form-name'>定制：</label>
				    	<label className='checkbox-inline'>
	                        <div className='checker' ><span><input name='lightvirtual' type='checkbox' value='1' /></span></div> 轻型服务器虚拟化
	                    </label>
					    <label className='checkbox-inline'>
	                        <div className='checker' ><span><input name='deployment' type='checkbox' value='1' /></span></div> 安装部署服务
	                    </label>
						<label className='checkbox-inline'>
	                        <div className='checker' ><span><input name='thirdParty' type='checkbox' value='1' /></span></div> 第三方软件
	                    </label>
                    </div>
				</form>
	}
});
var ProductBudgetForm = React.createClass({
	render:function(){
		return <form className='solution-bot-form'>
			      	<div className='input-group col-md-4'>
				  		<input type='text' className='form-control col' name='budget'/>
				  		<span className='input-group-addon'>万</span>
				  	</div>
			</form>;
	}
});
//var ContactInfoForm = React.createClass({
//	render:function(){
//		return <form className='solution-bot-form'>
//			    <div className='field-box'>
//			        <label className='form-name'>公司名称:</label>
//			        <input className='form-control' type='text' name='company'/>
//			    </div>
//				<div className='field-box'>
//			        <label className='form-name'>联系人:</label>
//			        <input className='form-control' type='text' name='contact'/>
//			    </div>
//				<div className='field-box'>
//			        <label className='form-name'>电话:</label>
//			        <input className='form-control' type='text' name='phone'/>
//			    </div>
//				<div className='field-box'>
//			        <label className='form-name'>邮箱:</label>
//			        <input className='form-control' type='text' name='email'/>
//			    </div>
//			</form>;
//	}
//});
var steps = [
	{
		title:<span>场景选择</span>,
		content:<SceneForm/>
	},{
		title:<span>软硬件配置</span>,
		content:<SoftHardWareForm/>
	},{
		title:<span>预算</span>,
		content:<ProductBudgetForm/>
	}
];

var wizard = ReactDOM.render(
		<FormWizard steps={steps} ></FormWizard>,
		document.getElementById('solution-wizard')
);

wizard.props.finish = function(){
	
	var unit = $('input[name="sceneUnit"]:checked').next().text();
	var unit2 = $('input[name="sceneUnit"]:checked').val();
	var application = $('input[name="sceneApplication"]:checked').next().text();
	var application2 = $('input[name="sceneApplication"]:checked').val();
	var point = $('input[name="scenePoint"]:checked').next().text();
	var point2 = $('input[name="scenePoint"]:checked').val();
	var pointL,pointR;
	if(point2 != null){
		var strs = point2.split('-');
		if(strs.length == 2 ){
			pointL = strs[0];
			pointR = strs[1];
		}else{
			console.error('单间点数异常：'+strs);
		}

	}
	
	
	var softwares = '';
	var softwares2 = [];
	$('input[name="softWare"]:checked').each(function(){
		softwares += $(this).next().text() + ',';
		softwares2.push($(this).val());
	});
	var terminal = $('input[name="terminal"]:checked').next().text();
	var terminal2 = $('input[name="terminal"]:checked').val();
	var hardwares = '';
	var hardwares2 = [];
	$('input[name="hardWare"]:checked').each(function(){
		hardwares += $(this).next().text() + ',';
		hardwares2.push($(this).val())
	});
	
	var lightvirtual = '无';
	var lightvirtual2 = 0;
	if($('input[name="lightvirtual"]:checked').length > 0){
		lightvirtual = '有';
		lightvirtual2 = 1;
	}
	var deployment = '无';
	var deployment2 = 0;
	if($('input[name="deployment"]:checked').length > 0){
		deployment = '有';
		deployment2 = 1;
	}
	var thirdParty = '无';
	var thirdParty2 = 0;
	if($('input[name="thirdParty"]:checked').length > 0){
		thirdParty = '有';
		thirdParty2 = 1;
	}
	
	var budget = $('input[name="budget"]').val()*10000;
	
	$('#sf-unit').text(unit);
	$('#sf-application').text(application);
	$('#sf-point').text(point);
	$('#sf-sotfware').text(softwares);
	$('#sf-terminal').text(terminal);
	$('#sf-hardware').text(hardwares);
	$('#sf-lightvirtual').text(lightvirtual);
	$('#sf-deployment').text(deployment);
	$('#sf-thirdParty').text(thirdParty);
	$('#sf-budget').text(budget);
	
	if(budget == null || budget == ''){
		layer.msg('项目预算不能为空')
		return;
	}else if(!numberValidator(budget,2)){
		layer.msg('项目预算只能为不小于0的整数')
		return;
	}else if(  !(30000 < budget && budget < 10000000)){
		layer.msg('项目预算最好在3~1000万之间')
		return;
	}
	
	layer.open({
		  type: 1,
		  area: ['750px','700px'],
		  title: '需求一览', 
		  content: $('#solution-from'), 
		  btn: ['提交', '再想想'],
		  closeBtn :false,
		  yes: function(index, layero){
			  $.ajax({
				  url:CTX_PATH + '/solution/match',
				  traditional :true,  
				  data:{
					  unit:unit2,
					  application:application2,
					  pointL:pointL,
					  pointR:pointR,
					  softWares:softwares2,
					  terminal:terminal2,
					  hardWares:hardwares2,
					  lightVirtual:lightvirtual2,
					  deploymentService:deployment2,
					  thirdSoftware:thirdParty2,
					  budget:budget
				  },
				  success:function(rtn){
					  console.log(rtn);
					  if(!rtn.error){
						  if(rtn.data != null){
							  
							  var solution= rtn.data;
							  var solutionId = solution.id;
							  $('#ms-service-cpu').text(solution.cpu);
							  $('#ms-service-memory').text(solution.cloudServiceMemory);
							  $('#ms-termianl-memory').text(solution.cloudTerminalMemory);
							  $('#ms-flash-memory').text(solution.flashStorage);
							  $('#ms-terminal').text(solution.frameWork);
							  $('#ms-disk').text(solution.hardDisk);
							  $('#ms-cabinet').text(solution.cabinet);
							  $('#ms-interface').text(solution['interface']);
							  $('#ms-monitor').text(solution.monitor);
							  $('#ms-mouce').text(solution.mouse);
							  $('#ms-power').text(solution.powerSupply);
							  $('#ms-radi').text(solution.radio);
							  $('#ms-router').text(solution.router);
							  $('#ms-switcher1').text(solution.switch1);
							  $('#ms-switcher2').text(solution.switch2);
							  
							  layer.open({
								  type: 1,
								  area: ['750px','650px'],
								  title: '匹配方案', 
								  content: $('#match-solution'), 
								  btn: ['申请试用'],
								  yes:function(index, layero){
									  layer.close(index)
									  var parentIndex = index;
									  //判断是否登录，如果登录且用户是代理商则获取当前用户的代理商信息，自动填表
									  //清空联系人表单
									  $('input[name="company"]').val(null);
									  $('input[name="contact"]').val(null);
									  $('input[name="phone"]').val(null);
									  $('input[name="email"]').val(null);
									  
									  console.log($('#apply-demand'))
									  console.log($('#solution-from'))
									  $('#apply-demand').html($('#solution-from table').clone());
									  $('#apply-solution').html($('#match-solution table').clone());
									  
									  layer.open({
										  type: 1,
										  area: ['600px','600px'],
										  title: '申请信息', 
										  content: $('#apply'), 
										  btn: ['确认'],
										  yes:function(index, layero){
											  var company = $('input[name="company"]').val();
											  var contact = $('input[name="contact"]').val();
											  var phone = $('input[name="phone"]').val();
											  var email = $('input[name="email"]').val();
											  
											  if((email == null || email == '') || (phone == null || phone == '')){
												  layer.msg('"邮箱"和"电话"不能为空，方便及时通知您申请结果');
											  }else if(phone != null && phone != '' && !phoneValidator(phone)){
												  layer.msg('您的手机号好像输错喽，请使用有效的手机号')
											  }else if(email != null && email != '' && !emailValidator(email)){
												  layer.msg('您的邮箱好像输错喽，请使用有效的邮箱')
											  }else{
												  $.ajax({
													  url:CTX_PATH + '/solution/apply',
													  traditional :true,  
												  	  data:{
												  		  token:$.cookie('token'),
												  		  solutionId:solutionId,
												  		  contact:contact,
												  		  email:email,
												  		  phone:phone,
												  		  company:company,
												  		  unit:unit2,
												  		  application:application2,
												  		  pointL:pointL,
												  		  pointR:pointR,
												  		  softWares:softwares2,
												  		  terminal:terminal2,
												  		  hardWares:hardwares2,
												  		  lightVirtual:lightvirtual2,
												  		  deploymentService:deployment2,
												  		  thirdSoftware:thirdParty2,
												  		  budget:budget,
												  	  },
												  	  success:function(rtn){
												  		  console.log('appply:%o',rtn);
												  		  if(!rtn.error){
												  			 layer.close(index);
												  			 /*$('#apply-result .apply-code').html(rtn.data);*/
												  			 layer.open({
																  type: 1,
																  area: ['400px','250px'],
																  title: false, 
																  content: $('#apply-result'), 
																  closeBtn:false,
																  btn:['返回首页','重新申请'],
																  btn1:function(){
																	  window.location.href = CTX_PATH;
																  },
																  btn2:function(){
																	  location.reload();
																  }
												  			 });
												  		  }else{
												  			  var msg;
												  			  if(rtn.error == 'TEOPS-009000'){
												  				msg = '申请失败，'+rtn.msg;
												  			  }else{
												  				msg = '申请失败，请稍后再试';
												  			  }
												  			  layer.msg(msg);
												  			  console.error(rtn.error);
												  		  }
												  	  }
												  });
											  }
											  
											  
										  }
									  });
								  }
							  });
							  
						  }else{
							  layer.msg('无匹配方案');
						  }
					  }else{
						  layer.msg('获取匹配方案失败：'+rtn.msg);
					  }
				  }
			  });
			  
			  layer.close(index);
		  }
		  ,btn2: function(index, layero){
			  layer.close(index);
		  }
	});
}

