//页面加载成功后弹出登录框
var loginLayer;
$(function(){
	loginLayer = layer.open({
		  title:'登录验证',
		  type: 1,
		  area: ['420px','250px'],
		  closeBtn: 0,
		  shade:[1, '#000'],
		  move: false,
		  content: $('#ssh-login') //这里content是一个DOM，注意：最好该元素要存放在body最外层，否则可能被其它的相对元素所影响
		});
});

var state = 0;//未连接
var socket;
var term;


function termHeight(){
	return parseInt(($(window).height() - 54)/18);
}
function termWidth(){
	return parseInt($(window).width()/8);
}
$('#connect').click(function(){
	var user = $("#user").val();
	var pwd = $("#pwd").val();
	var pwd2 = $("#admin0-pwd").val();
	if(user == null || user == ''){
		layer.msg('用户名不能为空');
		return;
	}
	if(pwd == null || pwd == ''){
		layer.msg('密码不能为空');
		return;
	}
	if(pwd2 == null || pwd2 == ''){
		layer.msg('admin0密码不能为空');
		return;
	}
	connect(user,pwd,pwd2);
})
/**
 * 
 * @returns
 */
var stompClient =null;
var loading = null;
function connect(usr,pwd,adminPwd){	
	loading = layer.load(1, {
		  shade: [0.5,'#fff'] //0.1透明度的白色背景
	});
	var url = "ws://"+window.location.host+"/teops/ws/ssh";
	if ('WebSocket' in window) {  
			socket = new WebSocket(url);  
    } else if ('MozWebSocket' in window) {  
        socket = new MozWebSocket(url);  
    }else{
    	messageBox("warning","浏览器不支持此功能");
    	return;
    }
	socket.onopen = function(event){
		layer.msg("连接服务器成功");
		var data = {
			action:"connect",
			content:{
				token:$.cookie('token'),
				sshUser:usr,
				sshPwd:pwd,
				admin0Pwd:hex_md5(adminPwd),
				hostId:nodeId[0]
			}
		}
		var dataStr = JSON.stringify(data);
		socket.send(dataStr);
	};
	socket.onmessage = function(event) {
	   var data = eval("("+event.data+")");
	   switch(data.code){
	   case -2:
		   if(loading != null){
			   layer.close(loading) 
		   }
		   layer.msg("连接异常")
		   break;
	   case -1:
		   if(loading != null){
			   layer.close(loading) 
		   }
		   layer.msg("连接失败")
		   break;
	   case 1:
			layer.msg("正在连接终端，请稍后...");
		    state = 1;
		   break;
	   case 2:
		   if(loginLayer != null){
			   layer.close(loginLayer); 
		   }
		   if(loading != null){
			   layer.close(loading) 
		   }
		   if(term == null){
			   initTerm();
		   }
		   term.write(data.data);
		   break;
	   }
	   
	};
	// 监听Socket的关闭
	socket.onclose = function(event) { 
		layer.msg("连接关闭")
		state = 2;
	}; 
 }
function initTerm(){
	term = new Terminal(termWidth(), termHeight(), function(key) {
		if(state == 1){
			var data = {
				action:"cmd",
				content:key
			}
			var dataStr = JSON.stringify(data);
			socket.send(dataStr);
		}
	}); 
	term.open();
	$('.terminal').detach().appendTo('#term');
	$(window).resize(function() {
		term.resize(termWidth(),termHeight());
	});
}
//
//window.onbeforeunload = function(){
//    var n = window.event.screenX - window.screenLeft;
//    var b = n > document.documentElement.scrollWidth - 20;
//    if (!(b && window.event.clientY < 0 || window.event.altKey)) {
//        //window.event.returnValue = "真的要刷新页面么？";
//    	if(state == 1 && socket != null){
//    		var data = {
//				action:"disconnect",
//				content:""
//			}
//	    	var dataStr = JSON.stringify(data);
//			socket.send(dataStr);
//    	}
//    }
//}

