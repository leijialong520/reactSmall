
var hostTable;
var hostTH = ['名称','节点','操作'];
/** 组件 **/
var HostTableTr = React.createClass({
	render:function(){
		var operationComp = <HostTableTrOperationGroup nodeId={this.props.nodeId} hostName={this.props.vmName}></HostTableTrOperationGroup>
		return <tr >
					<td >{this.props.vmName}</td>
					<td >{this.props.node}</td>
					<td>{operationComp}</td>
			  </tr>;
	}
});
var HostTableTrOperationGroup = React.createClass({
	restart:function(){
		// 重启虚拟机
		$.ajax({
			url:CTX_PATH+'/console/host/restart',
			type:'post',
			data:{
				token:$.cookie('token'),
				hostName:this.props.hostName,
				nodeId:this.props.nodeId
			},
			success:function(rtn){
				console.log('host restart data:%o',rtn)
				if(!rtn.error){
					layer.msg('重启云电脑发送成功')
				}else{
					layer.msg('重启云电脑发送失败');
					console.error(rtn.msg);
				}
			}
		})
	},
	link:function(){
		//TODO 连接虚拟机
		alert('连接云电脑 '+this.props.nodeId)
	},
	render:function(){
		return <div className='btn-group'>
		        <button type='button' onClick={this.restart} className='btn btn-default'>重启</button>
		      </div>;
	}
});



/** 获取云电脑 **/
function loadHostTableData(keyWords,pn,ps){
	var hostName;
	if(keyWords != null){
		hostName = keyWords.hostName;
	}
	$.ajax({
		url: CTX_PATH + '/console/host/page',
		type:'post',
		data:{
			token:$.cookie('token'),
			pageNum:pn,			
			pageSize:ps,
			name:hostName
		},
		success:function(rtn){
			console.log('host data:%o',rtn)
			if(!rtn.error){
				var data = rtn.data;
				var rows = [];
				for(var i in data){
					var r = data[i];
					rows.push(<HostTableTr vmName={r.vmName} node={r.nodeName} nodeId={r.nodeId} ></HostTableTr>);
				}
				hostTable.setState({ 
					'keyWords':{
						hostName:hostName
					},
					'rows':rows,
					'pageNum':pn,
					'pageSize':ps,
					'total':null
				});
			}else{
				layer.msg('数据加载失败');
				console.error(rtn.msg);
			}
		}
	});
}
function initHostTable(){
	
	var barComp = [
		<TeOpsTableSearch placeholder='搜索主机名...' enterCall={searchCall} ></TeOpsTableSearch>,
		<TeOpsTableBtn name='搜索' type='primary' icon='icon-search'  click={searchHost}></TeOpsTableBtn>,
		<TeOpsTableBtn name='重置' type=''   click={resetTable}></TeOpsTableBtn>
	];
	hostTable = ReactDOM.render(
			<TeOpsTable title='我的电脑' heads={hostTH}  paging={false} loadData={loadHostTableData} barCompnents={barComp}></TeOpsTable>,
			document.getElementById('host-table')
	);
	loadHostTableData();
}
function searchCall(val){
	var initState = hostTable.getInitialState();
	var keyWords = initState.keyWords;
	keyWords.hostName = val;
	loadHostTableData(keyWords,initState.pageNum,initState.pageSize);
}
function resetTable(){
	var initState = hostTable.getInitialState();
	loadHostTableData(null,initState.pageNum,initState.pageSize);
}
function searchHost(){
	var val;
	val = $("#host-table .search").val();
	searchCall(val)
}
/** 页面初始化方法 **/
initPageDefault();
initHostTable();