initPageDefault();
var appTable;
var appTH = ['应用名称'];
/**组件**/
var AppTableTr = React.createClass({
	render:function(){
		return <tr>
					<td>{this.props.appName}</td>
				</tr>;
	}
});
/**获取已购买应用**/
function loadUserAppData(keyWords,pn,ps){
	var appName;
	if(keyWords != null){
		appName = keyWords.appName;
	}
	$.ajax({
		url:CTX_PATH + "/console/user/app",
		type:'post',
		data:{
			token:$.cookie('token'),
			pageNum:pn,			
			pageSize:ps,
			appName:appName
		},
		success:function(rtn){
			console.log('userApp data:%o', rtn);
			if(!rtn.error){
				var data = rtn.data;
				var list = data.list;
				var pn = data.pageNum;
				var ps = data.pageSize;
				var total = data.total;
				var rows = [];
				for(var i in list){
					var r = list[i];
					rows.push(<AppTableTr appName ={r.appName}></AppTableTr>);
				}
				appTable.setState({ 
					'keyWords':{
						appName:appName
					},
					'rows':rows,
					'pageNum':pn,
					'pageSize':ps,
					'total':total
				});
			}else{
				layer.msg('数据加载失败');
				console.error(rtn.msg);
			}
		}
	});
}
function initUserAppTable(){
	var barComp = [
		<TeOpsTableSearch placeholder='搜索应用名称...' enterCall={searchCall} ></TeOpsTableSearch>,
		<TeOpsTableBtn name='搜索' type='primary' icon='icon-search'  click={searchApp}></TeOpsTableBtn>,
		<TeOpsTableBtn name='重置' type=''   click={resetTable}></TeOpsTableBtn>
	];
	appTable = ReactDOM.render(
			<TeOpsTable title='我的应用' heads={appTH}  paging={true} loadData={loadUserAppData} barCompnents={barComp}></TeOpsTable>,
			document.getElementById('app-table')
	);
	loadUserAppData();
}
function searchCall(val){
	var initState = appTable.getInitialState();
	var keyWords = initState.keyWords;
	keyWords.appName = val;
	loadUserAppData(keyWords,initState.pageNum,initState.pageSize);
}
function resetTable(){
	var initState = appTable.getInitialState();
	$(".search").val("");
	loadUserAppData(null,initState.pageNum,initState.pageSize);
}
function searchApp(){
	var val;
	val = $(".search").val();
	searchCall(val)
}
initUserAppTable();
loadUserAppData();