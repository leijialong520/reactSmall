$('.back-page').click(function(e){
	e.stopPropagation();
	e.preventDefault();
	window.history.go(-1);
});
/****组件******/
var TaskTableTr = React.createClass({
	render:function(){
		var time,node,task,state;
		return <tr className='task-tr'>
					<td >{this.props.time}</td>
					<td >{this.props.node}</td>
					<td >{this.props.task}</td>
					<td >{this.props.state}</td>
			  </tr>;
	}
});
var taskTable;
var head=['时间','节点','操作','状态'];
/**
 * 
 */
function initTaskTable(){
	taskTable = ReactDOM.render(
		<TeOpsTable  heads={head} paging={true} loadData={loadTaskTableData} ></TeOpsTable>,
		document.getElementById('task-table')
	);
	var initState = taskTable.getInitialState();
	loadTaskTableData(null,initState.pageNum,initState.pageSize);
}
/**
 * 加载任务列表数据
 */
function loadTaskTableData(keyWords,pn,ps){
	$.ajax({
		url:CTX_PATH + '/user/task/page',
		type:'post',
		data:{
			token:$.cookie('token'),
			pageNum:pn,
			pageSize:ps,
		},
		success:function(rtn){
			console.log('task data:%o',rtn);
			if(!rtn.error){
				var data = rtn.data;
				var list = data.list;
				var pn = data.pageNum;
				var ps = data.pageSize;
				var total = data.total;
				var rows = [];
				
				for(var i in list){
					var r = list[i];
					var time,state;
					switch(r.status){
					case -3:
						time = r.excutedTime;
						state = <span className='badge warn'>未实现</span>;
						break;
					case -2:
						time = r.excutedTime;
						state = <span className='badge danger'>失败</span>;
						break;
					case -1:
						time = r.excutedTime;
						state = <span className='badge danger'>失败</span>;
						break;
					case 1:
						time = r.submitDate;
						state = <span className='badge'>等待</span>;
						break;
					case 2:
						time = r.excutedTime;
						state = <span className='badge success'>成功</span>;
						break;
					}
					time = new Date(time).format('yyyy-MM-dd hh:mm:ss');
					rows.push(<TaskTableTr node={r.executor} task={r.name} time={time} state={state} />);
				}
				taskTable.setState({ 
					'keyWords':{
					},
					'rows':rows,
					'pageNum':pn,
					'pageSize':ps,
					'total':total
				});
			}
		}
	});
}

initTaskTable();