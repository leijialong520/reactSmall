$('.back-page').click(function(e){
	e.stopPropagation();
	e.preventDefault();
	window.history.go(-1);
});
/** **组件***** */
var BindMail = React.createClass({
	bindmail:function (){
		var val;
		val = $('#email').val();
		var email = val;
		loadBindMail(email);
	},
	render:function(){
		var inputStyle = {
			height:200,
		};
		var mailStyle ={
			width:300,
			height:30,
			display:'inline',
		}
		return <div className = 'col-md-7'>
					<div className = 'mail-box-center'>
						<input type='text' className ='form-control' id='email' style={mailStyle}  placeholder='请输入邮箱'/>
					</div>
					<div className = 'mail-box-bot'>
						<button type='button' className = 'btn-glow primary' onClick={this.bindmail} >提交</button>
					</div>
				</div>
	}
	
});
ReactDOM.render(
		<BindMail />,
		document.getElementById('user-bind')
);
function loadBindMail(email){
		var email;
		$.ajax({
			url:CTX_PATH + '/user/bind/email',
			type:'post',
			data:{
				token:$.cookie('token'),
				email:email
			},
			success:function(rtn){
				if(rtn.msg == "success"){
					layer.msg('发送成功');
				}else{
					layer.msg('您已经绑定，或者请您核对邮箱地址！');
				}
				console.log('host data:%o',rtn)
				ReactDOM.render(
						<BindMail />,
						document.getElementById('user-bind')
				);
			}
	});
}