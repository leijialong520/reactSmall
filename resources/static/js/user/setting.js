$('.back-page').click(function(e){
	e.stopPropagation();
	e.preventDefault();
	window.history.go(-1);
});
/** **组件***** */
var DetailInfo = React.createClass({
	render:function(){
		var settingpanel = {
			 padding: 40
		}
		return <div className='col-md-4'>
				<div className='settingpanel' style = {settingpanel}>
			        <ul className='list-group'>
				        <li className='list-group-item'>用户名：   {this.props.userName}</li>
			            <li className='list-group-item'>手机号：   {this.props.phone}</li>
			            <li className='list-group-item'>qq号码：  {this.props.qq}</li>
			            <li className='list-group-item'>邮箱账号： {this.props.email}</li>
			            <li className='list-group-item'>云点数：   {this.props.score}</li>
			        </ul>
			    </div>
			</div>
			}
});
var AddBindPhoneInfo = React.createClass({
	bindphone:function(){
		location.href =CTX_PATH +'/user/phone';
	},
	render:function(){
		var paddingStyle = {
			marginLeft:40
		}
		var settingpanel = {
			 padding: 40
		}
		return <div className='col-md-4'>
			        <div className='settingpanel' style={settingpanel}>
				        <ul className='list-group'>
				            <li className='list-group-item'>用户名：   {this.props.userName}</li>
				            <li className='list-group-item'>手机号：您未绑定手机号  <input type='button' className='btn-glow primary' style= {paddingStyle} value='立即绑定' onClick={this.bindphone}/></li>
				            <li className='list-group-item'>qq号码：  {this.props.qq}</li>
				            <li className='list-group-item'>邮箱账号： {this.props.email}</li>
				            <li className='list-group-item'>云点数：   {this.props.score}</li>
				        </ul>
			    </div>
			</div>
			}
});
var AddBindMailInfo = React.createClass({
	bindmail:function(){
		location.href =CTX_PATH +'/user/mail';
	},
	render:function(){
		var paddingStyle = {
			marginLeft:40
		}
		var settingpanel = {
			 padding: 40
		}
		return <div className='col-md-4'>
			      <div className='settingpanel' style={settingpanel}>
			        <ul className='list-group'>
			            <li className='list-group-item'>用户名：   {this.props.userName}</li>
			            <li className='list-group-item'>手机号：   {this.props.phone}</li>
			            <li className='list-group-item'>qq号码：  {this.props.qq}</li>
			            <li className='list-group-item'>邮箱账号：您未绑定邮箱 <input type='button' className='btn-glow primary' style= {paddingStyle} value='立即绑定' onClick={this.bindmail}/></li>
			            <li className='list-group-item'>云点数：   {this.props.score}</li>
			        </ul>
			    </div>
			</div>
			}
});
var AddMailAndPhoneInfo = React.createClass({
	bindphone:function(){
		location.href =CTX_PATH +'/user/phone';
	},
	bindmail:function(){
		location.href =CTX_PATH +'/user/mail';
	},
	render:function(){
		var paddingStyle = {
				marginLeft:40
		}
		var settingpanel = {
			 padding: 40
		}
		return <div className='col-md-4'>
			        <div className='settingpanel' style={settingpanel}>
				        <ul className='list-group'>
				            <li className='list-group-item'>用户名：   {this.props.userName}</li>
				            <li className='list-group-item'>手机号：您未绑定手机号  <input type='button' className='btn-glow primary' style= {paddingStyle} value='立即绑定' onClick={this.bindphone}/></li>
				            <li className='list-group-item'>qq号码：  {this.props.qq}</li>
				            <li className='list-group-item'>邮箱账号：您未绑定邮箱 <input type='button' className='btn-glow primary' style= {paddingStyle} value='立即绑定' onClick={this.bindmail}/></li>
				            <li className='list-group-item'>云点数：   {this.props.score}</li>
				        </ul>
				    </div>
				</div>
			}
});

// 加载数据
function loadDetailInfo(){
	$.ajax({
		url:CTX_PATH + '/user/info',
		type:'post',
		data:{
			token:$.cookie('token')
		},
		success:function(rtn){
			console.log('task data:%o',rtn);
			var userName,phone,qq,email,score;
			if(!rtn.error){
				var data = rtn.data;
				userName = data.userName;
				phone = data.phone;
				qq =data.qq;
				email = data.email;
				score = data.score;
			}
				if((phone == '' || phone == null) && (email == '' || email == null) ){
					ReactDOM.render(
							<AddMailAndPhoneInfo userName = {userName} phone = {phone} qq = {qq} email = {email} score = {score} />,
							document.getElementById('user-info')
					);
				}else if(email == '' || email == null){
					ReactDOM.render(
							<AddBindMailInfo userName = {userName} phone = {phone} qq = {qq} email = {email} score = {score} />,
							document.getElementById('user-info')
					);
				}else if(phone == '' || phone == null){
					ReactDOM.render(
						<AddBindPhoneInfo userName = {userName} phone = {phone} qq = {qq} email = {email} score = {score} />,
							document.getElementById('user-info')
					);
				}else{
					ReactDOM.render(
						<DetailInfo userName = {userName} phone = {phone} qq = {qq} email = {email} score = {score} />,
						document.getElementById('user-info')
					);
				}
			
		} 
	});
}
loadDetailInfo();