$('.back-page').click(function(e){
	e.stopPropagation();
	e.preventDefault();
	window.history.go(-1);
});
/** **组件***** */
var BindPhone = React.createClass({
	bindphone:function (){
		var val;
		val = $('.form-control').eq(0).val();
		var phone = val;
		loadBindPhone(phone);
		settime($('.btn-glow primary').eq(0)); //开始倒计时
	},
	vlidatecode:function (){
		var code;
		code = $('.form-control').eq(1).val();
		validateCode(code);
	},
	render:function(){
			return <div className='col-md-6'>
						<div className='phone-box'>
							<div className = 'phoneInfo'><p>手机号:</p></div>
						        <div className='col-md-7'>
						            <input className='form-control' id='phone' type='text' placeholder='请输入手机号'/>
						        </div>    
						</div>
						<div className = 'info-box'>
							<div className='code-box'>
								<div><p  className = 'phoneInfo'>验证码:</p></div>
							        <div className='col-md-7'>
							            <input className='form-control' type='text'/>
							        </div>
							        <button type='button' className = 'btn-glow primary' id ='codeBtn' onClick={this.bindphone} >获取验证码</button>	
							 </div>
						 </div>
						 <div className = 'validabtn'>
						 	<button type='button' className = 'btn-glow primary' onClick={this.vlidatecode} >立即验证</button>
						 </div>
				  </div>
		}
	
});
ReactDOM.render(
		<BindPhone/>,
		document.getElementById('user-bind')
);
function loadBindPhone(phone){
		var phone;
		if(phone != ''){
			$.ajax({
				url:CTX_PATH + '/user/bind/phone',
				type:'post',
				data:{
					token:$.cookie('token'),
					phone:phone
				},
				success:function(rtn){
					if(rtn.msg == 'success'){
						layer.msg('发送成功');
						addCookie('phone',phone,0);
						$('#phone').val(phone);
						countdown = getCookieValue('secondsremained')
						addCookie('secondsremained', 60, 60); //添加cookie记录,有效时间60s
				        settime($('#codeBtn')); //开始倒计时
					}else{
						layer.msg('您已经绑定，或者请您核对您的手机号！');
					}
					console.log('host data:%o',rtn)
					ReactDOM.render(
							<BindPhone />,
							document.getElementById('user-bind')
					);
				}
			});
		}else{
			layer.msg('请输入您的手机号！');
		}
}
$(function() {
	var v;
    v = getCookieValue('secondsremained'); //获取cookie值
    settime($('#codeBtn')); //开始倒计时
});

function validateCode(code){
	var code;
	if(code != ''){
		$.ajax({
			url:CTX_PATH + '/user/validate/phone',
			type:'post',
			data:{
				code:code
			},
			success:function(rtn){
				if(rtn.msg == 'success'){
					layer.msg('绑定手机号成功');		
				}else{
					layer.msg('绑定手机号失败');
				}
				console.log('host data:%o',rtn);
			}
		});
	}else{
		layer.msg('请输入手机验证码');
	}
}
//开始倒计时

var countdown;
function settime(obj) {
	var phone;
	phone = getCookieValue('phone');
	$('#phone').val(phone);
	if(obj != 'undefinded'){
		countdown = getCookieValue('secondsremained');
		if (countdown == 1 || countdown == undefined) {
			obj.removeAttr('disabled');
			obj.text('获取验证码');
			$.cookie('phone', '');
			return;
		} else {
			obj.attr('disabled', true);
				obj.text('(' + countdown + ')  重新发送');
				countdown--;
				editCookie('secondsremained', countdown, 0);
		}
		setTimeout(function() { settime(obj); }, 1000); //每1000毫秒执行一次
	}
}

function addCookie(name, value, expiresHours) {
    //判断是否设置过期时间,0代表关闭浏览器时失效
    if (expiresHours > 0) {
        var date = new Date();
        date.setTime(date.getTime() + expiresHours * 1000);
        $.cookie(name, escape(value), { expires: date });
    } else {
        $.cookie(name, escape(value));
    }
}

//修改cookie的值
function editCookie(name, value, expiresHours) {
    if (expiresHours > 0) {
        var date = new Date();
        date.setTime(date.getTime() + expiresHours * 1000); //单位是毫秒
        $.cookie(name, escape(value), { expires: date });
    } else {
        $.cookie(name, escape(value));
    }
}

//根据名字获取cookie的值
function getCookieValue(name) {
    return $.cookie(name);
}
				

				