
var hostTable;
var hostTH = ['基站名称','开始时间','共享时长','剩余时长','状态','签约人数','操作'];
/** 组件 **/
var ShareTableTr = React.createClass({
	render:function(){
		var state;
		if(this.props.state){
			state = <span className='label label-success'>已共享</span>
		}else{
			state = <span className='btn-flat gray'>已签约</span>
		}
		var createTime = formatDateTime(this.props.createTime);
		var overplusTime = timediff(this.props.overplusTime);
		var operationComp = <CancelShareNode hostId={this.props.hostId}></CancelShareNode>
		return <tr>
					<td>{this.props.hostName}</td>
					<td>{createTime}</td>
					<td>{this.props.shareTime}</td>
					<td>{overplusTime}</td>
					<td>{state}</td>
					<td>{this.props.signNum}</td>
					<td>{operationComp}</td>
			  </tr>;
	}
});
var CancelShareNode = React.createClass({
	cancel:function(){
		//取消共享
		$.ajax({
			url:CTX_PATH + '/share/node/cancel',
			type:'post',
			data:{
				token:$.cookie('token'),
				hostId:this.props.hostId
			},
			success:function(rtn){
				console.log('node cancel:%o',rtn)
				if(!rtn.error){
					layer.msg('取消基站共享成功');
					resetTable();
				}else{
					layer.msg('取消基站共享失败');
					console.error(rtn.msg);
				}
			}
		});
	},
	render:function(){
		return <div className='btn-group'>
		        <button type='button' onClick={this.cancel} className='btn-flat'>取消共享</button>
		      </div>;
	}
});

/** 获取已共享基站 **/
function loadShareNodeTableData(keyWords,pn,ps){
	var hostName;
	if(keyWords != null){
		hostName = keyWords.hostName;
	}
	$.ajax({
		url: CTX_PATH + '/share/node/list',
		type:'post',
		data:{
			token:$.cookie('token'),
			pageNum:pn,			
			pageSize:ps,
			hostName:hostName
		},
		success:function(rtn){
			console.log('shareNodeList data:%o',rtn)
			if(!rtn.error){
				var data = rtn.data;
				var list = data.list;
				var pn = data.pageNum;
				var ps = data.pageSize;
				var total = data.total;
				var hostId = data.hostId;
				var rows = [];
				for(var i in list){
					var r = list[i];
					rows.push(<ShareTableTr hostName={r.hostName} createTime={r.createTime} shareTime={r.time} overplusTime={r.overplusTime} signNum={r.signNum} state={r.state} hostId={r.hostId} ></ShareTableTr>);
				}
				hostTable.setState({ 
					'keyWords':{
						hostName:hostName
					},
					'rows':rows,
					'pageNum':pn,
					'pageSize':ps,
					'total':total
				});
			}else{
				layer.msg('数据加载失败');
				console.error(rtn.msg);
			}
		}
	});
}
/**
 * 时间转换
 */
function formatDateTime(timeStamp) {   
    var date = new Date();  
    date.setTime(timeStamp);  
    var y = date.getFullYear();      
    var m = date.getMonth() + 1;      
    m = m < 10 ? ('0' + m) : m;      
    var d = date.getDate();      
    d = d < 10 ? ('0' + d) : d;      
    var h = date.getHours();    
    h = h < 10 ? ('0' + h) : h;    
    var minute = date.getMinutes();    
    var second = date.getSeconds();    
    minute = minute < 10 ? ('0' + minute) : minute;      
    second = second < 10 ? ('0' + second) : second;     
    return y + '-' + m + '-' + d+' '+h+':'+minute+':'+second;      
};
//计算剩余多少时间  
function timediff(times){   
   var hours = parseInt(times / 1000 / 60 / 60, 10); //计算剩余的小时   
   var minutes = parseInt(times / 1000 / 60 % 60, 10);//计算剩余的分钟     
   var seconds = parseInt(times / 1000 % 60, 10);//计算剩余的秒数 
   hours = checkTime(hours);     
   minutes = checkTime(minutes);     
   seconds = checkTime(seconds);     
    var timehtml= hours+":" + minutes+":"+seconds; 
    return timehtml;  
}   
function checkTime(i){   
       //将0-9的数字前面加上0，例1变为01     
       if(i<10)   {   
       i = "0" + i;    
       }     
       return i;   
}  

function initShareNodeTable(){
	
	var barComp = [
		<TeOpsTableSearch placeholder='搜索基站名称...' enterCall={searchCall} ></TeOpsTableSearch>,
		<TeOpsTableBtn name='搜索' type='primary' icon='icon-search'  click={searchHost}></TeOpsTableBtn>,
		<TeOpsTableBtn name='重置' type=''   click={resetTable}></TeOpsTableBtn>
	];
	hostTable = ReactDOM.render(
			<TeOpsTable title='共享基站' heads={hostTH}  paging={true} loadData={loadShareNodeTableData} barCompnents={barComp}></TeOpsTable>,
			document.getElementById('share-table')
	);
	loadShareNodeTableData();
}
function searchCall(val){
	var initState = hostTable.getInitialState();
	var keyWords = initState.keyWords;
	keyWords.hostName = val;
	loadShareNodeTableData(keyWords,initState.pageNum,initState.pageSize);
}
function resetTable(){
	var initState = hostTable.getInitialState();
	$(".search").val("");
	loadShareNodeTableData(null,initState.pageNum,initState.pageSize);
}
function searchHost(){
	var val;
	val = $(".search").val();
	searchCall(val)
}
/** 页面初始化方法 **/
initPageDefault();
initShareNodeTable();